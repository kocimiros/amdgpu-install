#################################
Installing or Uninstalling AMDGPU
#################################

.. contents:: Table of Contents

----------

Installing the All-Open Variant
===============================
Run the following command to install the All-Open variant:

.. code-block:: bash

  $ ./amdgpu-install -y


.. note:: The ``-y`` option installs non-interactively. See
      :doc:`Using the amdgpu-install script <install-script>` for more
      information.

--------

Reboot the target system after running ``amdgpu-install``.

Installing the Pro Variant
==========================

Refer to the following table to understand how to install a combination of Pro
components:

+------------------------------------------------------------+-----------------+
|**Command**                                                 |**Installed      |
|                                                            |Components**     |
+------------------------------------------------------------+-----------------+
|``$ ./amdgpu-pro-install -y``                               |- Base kernel    |
|                                                            |- Accelerated    |
|                                                            |  graphics       |
|                                                            |- Mesa           |
|                                                            |  multimedia     |
|                                                            |- Pro OpenGL     |
|                                                            |- Pro Vulkan     |
+------------------------------------------------------------+-----------------+
|``$ ./amdgpu-pro-install -y --opencl=pal,legacy``           |- Base kernel    |
|                                                            |- Accelerated    |
|                                                            |  graphics       |
|                                                            |- Mesa           |
|                                                            |  multimedia     |
|                                                            |- Pro OpenGL     |
|                                                            |- Pro Vulkan     |
|                                                            |- Pro OpenCL     |
+------------------------------------------------------------+-----------------+
|``$ ./amdgpu-pro-install -y --opencl=pal,legacy --headless``|- Only base      |
|                                                            |  kernel         |
|                                                            |- Pro OpenCL     |
|                                                            |  (headless mode)|
+------------------------------------------------------------+-----------------+

Reboot the target system after running ``amdgpu-pro-install``.

For more information, refer to the following sections to understand how to
install individual components.

OpenGL (Default Component)
--------------------------

OpenGL is a default component of the Pro variant.

.. note:: Unless a headless installation is requested, OpenGL is installed.


OpenCL (Optional Component)
---------------------------
OpenCL is an optional component of the Pro variant and is installed only if it is
specifically requested.

Two different implementations of OpenCL (legacy and PAL) are provided.
Either of these implementations or both of them can be installed
on the target system.

In some scenarios, it may be desirable to install **only** the OpenCL portion
of the Pro variant (omitting the OpenGL portion), which can be accomplished
by adding the ``--headless`` option. The typical use case is headless compute.


Vulkan (Default Component)
---------------------------

Vulkan is a default component of the Pro variant.

.. note:: Unless a headless installation is requested, Vulkan is installed.


PX Platform Support (Ubuntu only)
------------------------------------
For PX (*PowerExpress*) platform support, use the ``--px`` option as shown in
the following command:

.. code-block:: bash

  $ ./amdgpu--pro-install --px

---------

.. note:: The All-Open variant supports PRIME GPU offloading which allows
      GPU workloads to be offloaded to a discrete GPU on demand, whereas
      the ``--px`` option is a static switch, requiring the user to
      restart X. Consequently, the ``--px`` option is neither required nor
      supported with the All-Open variant.


Uninstalling the AMDGPU Graphics Stack
======================================

To remove all components of the stack, run the uninstall script from anywhere
in the system.

1. Based on whether you installed the All-Open or Pro components, run one of
   the following commands:

   .. code-block:: bash

    # Use this for All-Open components
    $ amdgpu-uninstall

    # Use this for Pro components
    $ amdgpu-pro-uninstall


2. After uninstalling the components, reboot the target system.
